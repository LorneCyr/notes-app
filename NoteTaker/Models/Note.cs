using System;
using System.ComponentModel.DataAnnotations;

namespace NoteTaker.Models
{
    public class Note
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        [MaxLength(250)]
        public string NoteTitle { get; set; }
        
        [Required]
        public string NoteContents { get; set; }
    }
}