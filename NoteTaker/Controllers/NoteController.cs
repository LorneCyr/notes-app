using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NoteTaker.Data;
using NoteTaker.Dtos;

namespace NoteTaker.Controllers
{
    [Route("api/notes")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        private readonly INoteTakerRepo _repo;
        private readonly IMapper _mapper;

        public NoteController(INoteTakerRepo repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        // GET all Notes
        // GET api/notes
        [HttpGet]
        public ActionResult <IEnumerable<NoteReadDto>> GetAllNotes()
        {
            var noteItems = _repo.GetAllNotes();
            return Ok(_mapper.Map<IEnumerable<NoteReadDto>>(noteItems));
        }

        // GET note by id
        // GET api/notes/{id}
        [HttpGet("{id}", Name="GetNoteById")]
        public ActionResult <NoteReadDto> GetNoteById(int id)
        {
            var noteItem = _repo.GetNoteById(id);
            if(noteItem != null)
            {
                return Ok(_mapper.Map<NoteReadDto>(noteItem));
            }
            return NotFound();
        }

        // POST a new note
        // POST api/notes
        [HttpPost]
        public ActionResult <NoteReadDto> CreateNote(NoteCreateDto noteCreateDto)
        {
            var noteModel = _mapper.Map<NoteCreateDto>(noteCreateDto);
            _repo.CreateNote(noteModel);
            _repo.SaveChanges();

            var noteReadDto = _mapper.Map<NoteReadDto> (noteModel);

            return CreatedAtRoute(nameof(GetNoteById), new {Id = noteReadDto.Id}, noteReadDto);
        }

        // PATCH update note

        // DELETE note
    }
}