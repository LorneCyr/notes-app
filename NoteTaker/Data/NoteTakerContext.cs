using Microsoft.EntityFrameworkCore;
using NoteTaker.Models;

namespace NoteTaker.Data
{
    public class NoteTakerContext : DbContext
    {
        public NoteTakerContext(DbContextOptions<NoteTakerContext> opt) : base(opt)
        {
            
        }

        public DbSet<Note> Notes { get; set; }
    }
}