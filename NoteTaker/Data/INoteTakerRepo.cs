using System.Collections.Generic;
using NoteTaker.Models;

namespace NoteTaker.Data
{
    public interface INoteTakerRepo
    {
        bool SaveChanges();

        IEnumerable<Note> GetAllNotes();

        Note GetNoteById(int id);
        
        void CreateNote(Note note);
        void UpdateNote(Note note);
        void DeleteNote(Note note);

    }
}