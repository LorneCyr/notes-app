using System;

namespace NoteTaker.Dtos
{
    public class NoteCreateDto
    {
        public DateTime DateCreated { get; set; }

        public string NoteTitle { get; set; }
        
        public string NoteContents { get; set; }
    }
}