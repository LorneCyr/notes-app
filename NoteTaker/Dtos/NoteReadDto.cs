using System;

namespace NoteTaker.Dtos
{
    public class NoteReadDto
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public string NoteTitle { get; set; }
        
        public string NoteContents { get; set; }
    }
}